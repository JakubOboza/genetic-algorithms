set :deploy_via, :copy

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

set  :servers, %w( 198.199.126.17 )
role :app, *servers
role :web, *servers
role :db, *servers.first