require 'bundler/capistrano'
require "rvm/capistrano"
require 'capistrano/ext/multistage'
load 'deploy/assets'

set :rvm_ruby_string, '1.9.2-p320'

set :user, "appserver"
set :application, "genetic_algorithms"
set :repository,  "git@bitbucket.org:JakubOboza/genetic-algorithms.git"

set :scm, :git

set :deploy_to, "/var/www/#{application}"
set :rvm_path,  "/home/appserver/.rvm"
set :rvm_type, :user

set :default_stage, :autodeploy

namespace :deploy do
  task :restart do
    run "ruby #{current_path}/bin/run.rb stop"
    run "ruby #{current_path}/bin/run.rb start"
  end

  task :link_db_config do
    ["configuration.yaml"].each do |name|
      db_config = "#{release_path}/config/#{name}"
      shared_db_config = "#{shared_path}/config/#{name}"
      run "ln -s #{shared_db_config} #{db_config}"
    end
  end

  namespace :assets do

    task :precompile do
    end

    task :update_asset_mtimes do
    end

  end

end

namespace :bundle do
  task :install do
    run ""
  end
end

after "deploy:update_code", "deploy:link_db_config"